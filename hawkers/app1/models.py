from django.db import models
from django.contrib.auth.models import User
import datetime


class rest(models.Model):
    City = models.CharField(max_length=100)
    Address = models.CharField(max_length=250)

    def __str__(self):
        return self.City

    class Meta():
        verbose_name_plural = 'Restaurant Branches'