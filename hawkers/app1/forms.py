from django import forms
from django.contrib.auth.models import User
from app1.models import rest

class registerform(forms.ModelForm):
    
    class Meta():
        model = User
        fields = ('first_name','last_name','username','email','password')
        widgets={
            'first_name':forms.TextInput(attrs={'placeholder':'Enter First Name Here','label':'First Name'}),
            'last_name':forms.TextInput(attrs={'placeholder':'Enter Last Name Here','label':'Last Name'}),
            'username':forms.TextInput(attrs={'placeholder':'Enter desired Username Here','label':'Username'}),
            'email':forms.TextInput(attrs={'placeholder':'Enter E-Mail Name Here','label':'E-mail'}),         
            'password':forms.TextInput(attrs={'placeholder':'Choose a Strong Password','label':'Password','type':'password'}),
        }

class addressForm(forms.ModelForm):
    class Meta():
        model = rest
        fields = '__all__'