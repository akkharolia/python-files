from django.urls import path
from app1 import views


urlpatterns = [
    path('',views.homepage,name='home'),
    path('register/',views.userRegister,name='userregister'),
    path('staff/',views.stafflogin,name='stafflogin'),
    path('baseReg/',views.basicreg,name='basereg'),
    path('login/',views.uslogin,name='login'),
    path('uslogout/',views.uslogout,name="uslogout"),
    path('active/',views.user_active,name="active_user"),
]