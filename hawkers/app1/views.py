from django.shortcuts import render
import datetime
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.models import User
from django.http import request,HttpResponse,HttpResponseRedirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from app1.forms import registerform,addressForm
from app1.models import rest
from app1.tables import rest_table
# Create your views here.
def homepage(request):
    return render(request,'home.html')

def uslogin(request):
    user_status = User
    if request.method=='POST':
        usern = request.POST['un']
        pas = request.POST['pwd']

        check = authenticate(username=usern,password=pas)

        if check:
            if check.is_staff:
                login(request,check)
                response = HttpResponseRedirect(reverse('stafflogin'))
                return response
            elif check.is_active:
                login(request,check)
                response = HttpResponseRedirect(reverse('active_user'))
        else:
            return render(request,'login.html',{'status':'Invalid Login Details!!!','user_status':user_status})
    return render(request,'login.html',{'user_status':user_status})

def userRegister(request):
    if request.method=='POST':
        name = request.POST['name']
        un = request.POST['username']
        em = request.POST['email']
        pwd = request.POST['password']
        
        user = User.objects.create_user(un,em,pwd)
        user.first_name = name
        user.is_staff=True
        user.save()

        return render(request,'register.html',{'status':'Registered Successfully !!!'})
    return render(request,'register.html')

def basicreg(request):
    myform = registerform()
    if request.method=='POST':
        myform = registerform(request.POST)
        if myform.is_valid():
            user = myform.save()
            user.set_password(user.password)
            user.save()
            d={'frm':myform,'status':'Registred Successfully!!'}
            return render(request,'basicreg.html',d)
    return render(request,'basicreg.html',{'frm':myform})

@login_required  
def stafflogin(request):
    rest_data = rest.objects.all()
    form = addressForm()
    if request.method=='POST':
        form = addressForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request,'stafflogin.html',{'addf':form,'status':'Added Successfully!!'})
    return render(request,'stafflogin.html',{'addf':form,'rest_data':rest_data})

@login_required
def user_active(request):
    data = rest_table(rest.objects.all())
    return render(request,'active.html',{'rest_table':data})

@login_required
def uslogout(request):
    logout(request)
    return HttpResponseRedirect('/app1/home/')