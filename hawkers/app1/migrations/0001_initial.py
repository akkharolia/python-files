# Generated by Django 3.0.1 on 2019-12-19 12:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='rest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('City', models.CharField(max_length=100)),
                ('Address', models.CharField(max_length=250)),
            ],
            options={
                'verbose_name_plural': 'Restaurant Branches',
            },
        ),
    ]
