# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import HtmlResponse
from scrapy import Request

class QuotesSpider(scrapy.Spider):
    name = 'quotes'
    # allowed_domains = ['https://stackoverflow.com']
    start_urls = ["https://stackoverflow.com/questions"]

    def parse(self, response):
        new_list = []
        for test in response.xpath('//html/body'):
            data = test.xpath('.//a[@class="question-hyperlink"]/text()').extract()
            
            next_page = response.xpath('.//a[@rel="next"]/@href').extract()
            if next_page:
                next_href = next_page[0]
                start_urls = 'http://stackoverflow.com' + next_href
                request = scrapy.Request(url=start_urls)
                yield Request(start_urls, callback=self.parse)
                new_list.append(data)
                print(new_list)
                with open('qus.csv','w+',encoding='UTF-8') as file: 
                    file.write(str(new_list))
                        
                

