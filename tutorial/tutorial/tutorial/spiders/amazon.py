# -*- coding: utf-8 -*-
import scrapy
from scrapy import Spider
from scrapy.selector import Selector
from scrapy.http import HtmlResponse

class AmazonSpider(Spider):
    name = 'amazon'
    allowed_domains = ['duckduckgo.com']
    start_urls = ['https://en.wikipedia.org/wiki/Google']

    def parse(self, response):
        data = response.xpath('//*[@id="mw-content-text"]/div/p[3]/').extract()
        yield {'data':data}
