# -*- coding: utf-8 -*-
from scrapy import Spider
from scrapy.http import HtmlResponse

class GoogleSpider(Spider):
    name = 'google'
    allowed_domains = ['google.com']
    start_urls = ['https://play.google.com/store/apps/details?id=com.zhiliaoapp.musically&hl=en']

    def parse(self, response):
        # install = response.xpath("//div[contains(@class, 'hAyfc')][3]/span[contains(@class, 'htlgb')]/div[contains(@class, 'IQ1z0d')]/span[contains(@class, 'htlgb')]/text()").extract()
        email = response.xpath('''//a[contains(@class, 'hrTbp euBY6b')]/text()''').extract()
        yield {'email':email}