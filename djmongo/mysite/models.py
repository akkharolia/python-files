from djongo import models
from django.contrib.auth.models import User
# Create your models here.
class register(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    subtitle = models.CharField(max_length=200)
    dob = models.DateField()
    city = models.CharField(max_length=200)
    registred_on = models.DateTimeField(auto_now_add=True)
    profile_pic = models.ImageField(upload_to='media/%Y/%m/%d',blank=True)

    def __str__(self):
        return self.user.username