from django.shortcuts import render
from django.http import request,HttpResponse,HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login,logout,authenticate
from mysite.forms import registerForm
from mysite.models import register
# Create your views here.
def registerView(request):
    myform = registerForm()
    if request.method=="POST":
        myform = registerForm(request.POST)
        if myform.is_valid():
            user = myform.save()
            user.set_password(user.password)
            user.save()
            d={'frm':myform,'status':'Registred Successfully!!'}
            return render(request,'register.html',d)
    return render(request,'register.html',{'frm':myform})

def userReg(request):
        if request.method =="POST":
                name = request.POST['name']
                sub = request.POST['subtitle']
                un = request.POST['username']
                pwd = request.POST['password']
                dob = request.POST['dob']
                em = request.POST['email']
                city = request.POST['city']

                user = User.objects.create_user(un,em,pwd)
                user.first_name = name
                user.is_staff=True
                user.save()

                rg = register(user=user,subtitle=sub,dob=dob,city=city)
                rg.save()
                return render(request,'userReg.html',{'status':'Registred Successfully!!'})

        return render(request,'userReg.html')

import datetime
def uslogin(request):
        if request.method == "POST":
                usern = request.POST['un']
                pas = request.POST['pwd']

                check = authenticate(username=usern,password=pas)
                if check:
                        if check.is_staff:
                                login(request,check)
                                response=HttpResponseRedirect('/mysite/userdash')
                                if "rememberme" in request.POST:
                                        response.set_cookie('username',check.username)
                                        response.set_cookie('id',check.id)
                                        response.set_cookie('time',datetime.datetime.now())
                                return response
                                
                        elif check.is_active:
                                login(request,check)
                                return HttpResponseRedirect('/mysite/compdash')
                        else:
                                return render(request,'login.html',{'status':'Sorry Your Account is not activated to Login!!'})
                        
                else:
                        return render(request,'login.html',{'status':'Invalid Login Details!!!'})
        return render(request,'login.html')

@login_required
def compdash(request):
        log = User.objects.get(username=request.user.username)
        name = log.first_name[0]+log.last_name[0]
        return render(request,'compdash.html',{'name':name})

@login_required
def userdash(request):
        log = User.objects.get(username=request.user.username)
        
        return render(request,'userdash.html')


def uslogout(request):
        logout(request)
        response = HttpResponseRedirect('/')
        response.delete_cookie('id')
        response.delete_cookie('username')
        response.delete_cookie('time')
        return response

def validUser(request):
        if 'usn' in request.GET:
                name = request.GET['usn']
                check = User.objects.filter(username=name)
                if len(check)>=1:
                        return HttpResponse('Exists')
                else:
                        return HttpResponse('Not Exist')