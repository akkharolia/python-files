def Fibonnacci(givenNumber):
    number1 = 1
    number2 = 1
    nextFibo = number1 + number2
    while nextFibo <= givenNumber:
        number1 = number2
        number2 = nextFibo
        nextFibo = number1 + number2
        # print(nextFibo)

    return nextFibo

print(Fibonnacci(110))
