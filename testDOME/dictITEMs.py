def new_dict(dict_v):
    my_dict = dict([(i,[]) for i in set([value for key,value in dict_v.items()])])
    [my_dict[value].append(key) for key,value in dict_v.items()]
    return my_dict

print(new_dict(dict_v))

dict_v = {
    'Ajay':'Name',
    'Arun':'Name',
    'Hawkers':'Branch',
}